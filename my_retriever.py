import math

def dist_euclidean(a: list, b: list) -> float:
    """
    UNUSED
    Calculates the euclidean distance between the query vector and the document
    """
    summ = 0
    for i in range(len(a)):
        x = a[i] - b[i]
        summ += x * x
    return math.sqrt(summ)

def dist_cosine(q: list, d: list) -> float:
    """
    Calculates the cosine distance between the query vector and the document
    """
    dot = 0
    d_mag = 0
    for i in range(len(q)):
        dot += q[i] * d[i]
        d_mag += d[i] * d[i]
    
    d_mag = math.sqrt(d_mag)

    return dot / d_mag

class Retrieve:
    
    # Create new Retrieve object storing index and term weighting 
    # scheme. (You can extend this method, as required.)
    def __init__(self,index, term_weighting):
        self.index = index
        self.term_weighting = term_weighting
        self.num_docs = self.compute_number_of_documents()

        self.term_index = self.process_to_term_index(index)
        self.doc_index = self.process_to_doc_index(index)

    def process_to_term_index(self, index):
        term_index = {}
        for term, docs in self.index.items():
            data = {}
            data['df'] = len(docs)
            data['idf'] = math.log(self.num_docs / data['df'])
            cf = 0
            for id, count in docs.items():
                cf += count
            
            data['cf'] = cf
            data['docs'] = docs

            term_index[term] = data
        return term_index
    
    def process_to_doc_index(self, index):
        doc_index = {}
        for term, data in index.items():
            for id, tf in data.items():
                if not id in doc_index:
                    doc_index[id] = { }
                doc_index[id][term] = { 'tf': tf, 'tfidf': tf * self.term_index[term]['idf'] }
        return doc_index

    def compute_number_of_documents(self):
        self.doc_ids = set()
        for term in self.index:
            self.doc_ids.update(self.index[term])
        return len(self.doc_ids)

    # Method performing retrieval for a single query (which is 
    # represented as a list of preprocessed terms). Returns list 
    # of doc ids for relevant docs (in rank order).
    def for_query(self, query):
        if self.term_weighting == "binary":
            return self.binary(query)
        elif self.term_weighting =='tf':
            return self.term_freq(query)
        else: # tfidf
            return self.tfidf(query)

    def binary(self, query) -> list:
        """
        Performs binary ranking with the query against the set of documents

        returns: a list of relevant documents
        """
        docs = []
        for term in query:
            if term in self.term_index:
                for id, tf in self.term_index[term]['docs'].items():
                    if not id in docs:
                        docs.append(id)

        return docs
    
    def term_freq(self, query: list) -> list:
        """
        Performs term frequency ranking with the query against the set of documents

        returns: a list of relevant documents
        """
        docs = {}

        tfs_of_query = {}
        for term in query:
            if term in tfs_of_query:
                tfs_of_query[term] += 1
            else:
                tfs_of_query[term] = 1

        for id, doc_tf_map in self.doc_index.items():
            terms = set(query).union(set(doc_tf_map.keys()))
            q_vector = []
            d_vector = []
            for term in terms:
                q_vector.append(tfs_of_query[term] if term in tfs_of_query else 0)
                d_vector.append(doc_tf_map[term]['tf'] if term in doc_tf_map else 0)
            
            docs[id] = dist_cosine(q_vector, d_vector)
        
        items = list(docs.items())
        items.sort(reverse=True, key=lambda i : i[1])
        return [ i[0] for i in items ]
    
    def tfidf(self, query: list) -> list:
        """
        Performs TFIDF ranking with the query against the set of documents

        returns: a list of relevant documents
        """
        tfidfs_of_query = {}
        for term in query:
            if term in tfidfs_of_query:
                tfidfs_of_query[term] += 1
            else:
                tfidfs_of_query[term] = 1
        
        for term in tfidfs_of_query:
            if term in self.term_index:
                tfidfs_of_query[term] *= self.term_index[term]['idf']
            else:
                tfidfs_of_query[term] = 0
        
        q_terms = set(tfidfs_of_query.keys())
        
        docs = {}
        for id, doc_tf_map in self.doc_index.items():
            # Get the tfidf for each term
            terms = q_terms.union(set(doc_tf_map.keys()))
            q_vector = []
            d_vector = []
            for term in terms:
                q_vector.append(tfidfs_of_query[term] if term in tfidfs_of_query else 0)
                d_vector.append(doc_tf_map[term]['tfidf'] if term in doc_tf_map else 0)
            
            docs[id] = dist_cosine(q_vector, d_vector)

        items = list(docs.items())
        items.sort(reverse=True, key=lambda i : i[1])
        return [ i[0] for i in items ]
