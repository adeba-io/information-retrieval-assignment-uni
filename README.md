# Information Retrieval Uni Assignment

The purpose of this project was to implement a basic information retrieval system
using three IR methods - `binary`, `term frequency` and `tfidf`.

The file I wrote is `my_retriever.py`. It outputs a file `results.txt` which 
lists the most accurate documents for a given query.  
The queries are loaded from `queries.txt`, and the documents are loaded from `documents.txt`.  

I was provided with a `example_results_file.txt`, which I am told is the upper limit of what you can achieve on this task.  

You can see my best results by running `run_best.bat`. You can then compare it with the scores of the example results file by running `example_scores.bat`.
