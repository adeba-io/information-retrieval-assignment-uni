import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

figsize=(6, 5)

binary = {
	'none': { 'rel': 30,  'p': 0.05, 'r': 0.04, 'f': 0.04 },
	'ss': { 'rel': 35, 'p': 0.05, 'r': 0.04, 'f': 0.05 },
}

tf = {
	'none': { 'rel': 49, 'p': 0.08, 'r': 0.06, 'f': 0.07 },
	'ss': { 'rel': 122, 'p': 0.19, 'r': 0.15, 'f': 0.17 },
}

tfidf = {
	'none': { 'rel': 132, 'p': 0.21, 'r': 0.17, 'f': 0.18 },
	'ss': { 'rel': 172, 'p': 0.27, 'r': 0.22, 'f': 0.24 },
}

ex = { 'rel': 170, 'p': 0.27, 'r': 0.21, 'f': 0.24 }

fig = plt.figure(figsize=figsize)

relret = fig.add_subplot(111)
res = [ ex['rel'] ]
for i in [ binary, tf, tfidf ]:
	for j, y in i.items():
		res.append(y['rel'])

rects = relret.bar([ 'example', 'bin', 'bin w/ ss', 'tf', 'tf w/ ss', 'tfidf', 'tfidf w/ ss' ], res)
relret.set_title('Total Relevant Documents Retrieved')
plt.xlabel("Method")
relret.bar_label(rects)
fig.savefig('relretr.png')

fig = plt.figure(figsize=figsize)
stats = fig.add_subplot(111) 
data = [ ('example', ex['p'] * 100, ex['r'] * 100, ex['f'] * 100 ) ]
for d, name in [ (binary, 'bin' ), (tf, 'tf'), (tfidf, 'tfidf') ]:
	for key, v in d.items():
		data.append(( name + ('' if key == 'none' else ' w/ ' + key), v['p'] * 100, v['r'] * 100, v['f'] * 100 ))

x = np.arange(len(data))
width = 0.3
h_width = width / 2
b_precision = stats.bar(x - width - h_width, [ i[1] for i in data ], width, label='Precision')
b_recall = stats.bar(x - h_width, [ i[2] for i in data ], width, label='Recall')
b_fmeasure = stats.bar(x + width - h_width, [ i[3] for i in data ], width, label='F-Measure')

stats.set_ylabel('Scores, %')
stats.set_title('Scores of Each Method')
stats.set_xticks(x)
stats.set_xticklabels([ i[0] for i in data ])
stats.legend()
stats.bar_label(b_precision)
stats.bar_label(b_recall)
stats.bar_label(b_fmeasure)
fig.savefig('stats.png')
