# Assignment: Document Retrieval by Oluwatamilore Adebayo

## My Implementations

### Binary

Binary is the most simple of the three. I simply create a list, and add 
any and all documents that have a term in common with the query. Very simple.  
Something interesting I learned, that comes as a result of it's simplicity is that
the use of a set produces much, much poorer results. This is because when adding a 
number to a set using the `set.add()` function, it automatically sorts the set.

### Term Frequency

A bit more complex an implementation. First we get the term frequency of each term 
in the query. Then for each document, we compose a vector of counts for all the 
terms between them, putting zero when the query/document doesn't have it. 
Using these two vectors we then compute the cosine distance between them, storing that 
value in a dictionary with the document id as key so we can use it later.  
When we have done this for all the documents, we then sort the dictionary by its values.

## TFIDF

The most complex solution that builds off of term frequency. Upon creation of the 
`Retrieve` class we calculate the term informativeness of all terms in the class 
function `compute_term_informativeness`. This calculates the IDF value for each of 
the terms, which is `log( no. of documents / no. of documents containing w )` where `w` 
is the current term. The IDF is larger for terms that appear less frequently 
in the collection.  
Multiplying this value by the same term's frequency gives us the TFIDF. 
Now when we have the query, we calculate the TFIDF for each of its terms, 
and from there we finish in the same way as when calculating with term frequency, 
just replacing with the TFIDF where appropriate.


## Results

*Note that `ss` indicates stemming and stoplisting*

![Relevant Docs Retrieved Graph](relretr.png)

Above is a graph displaying the number of relevant retrieved documents by each of 
my implementations. They are compared with the same implementation now using both 
stemming and stoplisting and the example. For reference the total of relevant documents
in the set is 796.  

![Scores of Each Method](stats.png)

Above is another graph showcasing the precision, recall, and f-measure of each method. 
Included are scores when using stemming and stoplisting, and on the far left are the 
scores of the example file.


## Conclusion

It is very clear to see that the use of stemming and stoplisting greatly improves results 
across the board, the disparity being not so great with binary, most likely because of how 
random that method can be.  

Between binary, term frequency and TFIDF, the results of them get progressively better, with
TFIDF being clearly superior.
